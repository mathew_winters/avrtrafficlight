#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>
#include <avr/sleep.h>
#include <avr/power.h>
#include "cwdt.h"
#include "power.h"
#include "LedControl.h"
#include "arduino/Arduino.h"

#define WDT_COUNTDOWN 30

volatile uint16_t gWdtCount;

ISR(WDT_vect)
{
 // power::Instance()->wdt_isr();
  //gWdtCount++;
}

ISR (PCINT1_vect)
{
    /* interrupt code here */
}


//power::power(ISR_CALLBACK cb)
//{
//  callback = cb;
power::power()
{
}

power::~power()
{

}


void power::Init()
{
  gWdtCount = 0;
}

void power::startSleepInterrupt()
{
  cli();

  PORTC |= (1 << PORTC2); // black
  PORTC |= (1 << PORTC3); // red

  PCICR |= (1 << PCIE1);
  PCMSK1 |= (1 << PCINT10) | (1 << PCINT11);
  sei();

}

void power::stopSleepInterrupt()
{
  PCICR = 0;      // disable pin-change interrupts
  PCMSK1 = 0;
}


void power::enterSleep()
{


  //startSleepInterrupt();
  LedControl::allOff(); // all leds off.

  // set pins to input and LOW
  for(int x = 0 ; x <= 19 ; x++)
  {
    digitalWrite(x,LOW);
  }

  //set_sleep_mode(SLEEP_MODE_PWR_SAVE);


//  ADCSRA &= ~(1<<ADEN); //Disable ADC
//  ACSR = (1<<ACD); //Disable the analog comparator
//  DIDR0 = 0x3F; //Disable digital input buffers on all ADC0-ADC5 pins
//  DIDR1 = (1<<AIN1D)|(1<<AIN0D); //Disable digital input buffer on AIN1/0
  startSleepInterrupt();

  set_sleep_mode(SLEEP_MODE_PWR_DOWN);   /* EDIT: could also use SLEEP_MODE_PWR_DOWN for lowest power consumption. */

  power_adc_disable();
  power_twi_disable();
  power_spi_disable();
  power_usart0_disable();
  power_timer0_disable(); //Needed for delay_ms
  power_timer1_disable();
  power_timer2_disable();

  sleep_mode();//cpu();

  sei();

  stopSleepInterrupt();

  /* Re-enable the peripherals. */
  power_all_enable();

  LedControl::init();
  _delay_ms(50);
}

void power::wdt_isr()
{

}

