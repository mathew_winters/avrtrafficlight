local GCC_AVR_CC_PATH = "/usr/bin/avr-gcc"
local GCC_AVR_CPP_PATH = "/usr/bin/avr-g++"
local GCC_AVR_AR_PATH = "/usr/bin/avr-ar"

table.insert(premake.option.list["platform"].allowed, { "avr" })

premake.platforms.avr = {
	cfgsuffix = "avr",
	iscrosscompiler = true
}

table.insert(premake.fields.platforms.allowed, "avr")


if(_OPTIONS.platform == 'avr') then
	premake.gcc.cc = GCC_AVR_CC_PATH
	premake.gcc.cxx = GCC_AVR_CPP_PATH
	premake.gcc.ar = GCC_AVR_AR_PATH
end


solution "TrafficLight"
  configurations {"debug","Program"}
  platforms { "avr" ,"native"}
  project "TrafficLight"
	  targetname "TrafficLight.elf"
  	language "C++"
  	kind "ConsoleApp"

		local includeFiles = {
			"**.h",
			"**.c",
			"**.cc",
			"**/**.h",
			"**/**.c",
			"**/**.cc",
			"**/**.cpp",

		}
		files(includeFiles)

    defines { "AVR" }


  	defines { "F_CPU=8000000UL","NATIVE" ,"ARDUINO_MAIN"}
		buildoptions{"-Wall","-mcall-prologues","-mmcu=atmega328p","-std=gnu99","-Os","-g3"}
		linkoptions {
						"-mmcu=atmega328p",
						"-Wl,-gc-sections -Wl,-relax"
	 }

  	configuration "debug"
				targetdir "build/build/"
				objdir "build/build/obj"
				postbuildcommands { "avr-objcopy -R .eeprom -O ihex build/build/TrafficLight.elf build/build/TrafficLight.hex",
					"avr-size -C --mcu=atmega328p build/build/TrafficLight.elf"}

    configuration "Program"
        defines { "NDEBUG" }

				targetdir "build/program"
				objdir "build/program/obj"
				postbuildcommands {
				        "avr-objcopy -R .eeprom -O ihex build/program/TrafficLight.elf build/program/TrafficLight.hex",
								"avr-size -C --mcu=atmega328p build/program/TrafficLight.elf",
								"avr-objdump -h -S build/program/TrafficLight.elf  > build/program/TrafficLight.lss",
								"avrdude -q -p m328p -c avrisp2 -P /dev/ttyACM0 -U flash:w:build/program/TrafficLight.hex"
--								"putty -load avr3.3v &"
								}

--		configuration {"avr", "gmake" }
			-- Run shell commands before build is started
--			prebuildcommands { '@echo "\\n\\n--- Starting to build: `date` ---\\n\\n"' }

			-- Run shell commands after build is finished
--			postbuildcommands { '@echo "\\n\\n--- Finished build ---\\n\\n"' }

			-- Sets the right mmcu
--			buildoptions {"-fpack-struct -fshort-enums  -funsigned-char -funsigned-bitfields"}

