/*
 */
#include <avr/io.h>
#include <avr/interrupt.h>
//#include <util/delay.h>

#include "arduino/Arduino.h"

#include "power.h"
#include "LedControl.h"
#include "arduino/HardwareSerial.h"
#include "ClickButton.h"

#define MS_TOOFF 120000

power apower;
Leds led = ledRed;
// red C3
// black C2
#define BUTTON_RED 17
#define BUTTON_BLACK 16

ClickButton buttonRed(BUTTON_RED,LOW,CLICKBTN_PULLUP);
ClickButton buttonBlack(BUTTON_BLACK,LOW,CLICKBTN_PULLUP);
unsigned long msAlive = 0;

void setup(void)
{

  apower.Init();
  LedControl::init();
  LedControl::on(ledRed);

  delay(1000);
  LedControl::on(ledYellow);

  delay(1000);
  LedControl::on(ledGreen);

  delay(1000);
  LedControl::allOff();
  // Insert code
//  sei();

  Serial.begin(57600);
  Serial.println("Traffic Light");

  buttonRed.debounceTime   = 20;   // Debounce timer in ms
  buttonRed.multiclickTime = 250;  // Time limit for multi clicks
  buttonRed.longClickTime  = 1000;

  buttonBlack.debounceTime   = 20;   // Debounce timer in ms
  buttonBlack.multiclickTime = 250;  // Time limit for multi clicks
  buttonBlack.longClickTime  = 1000;

  msAlive = millis();
}



void ledForward()
{
  LedControl::off(led);
  switch(led)
  {

    case ledRed:
      led = ledGreen;
      break;
    case ledYellow:
      led = ledRed;
      break;
    case ledGreen:
      led = ledYellow;
      break;
  }
  LedControl::on(led);

}

void ledBackward()
{
  LedControl::off(led);
  switch(led)
  {

    case ledRed:
      led = ledYellow;
      break;
    case ledYellow:
      led = ledGreen;
      break;
    case ledGreen:
      led = ledRed;
      break;
  }
  LedControl::on(led);

}

int redClicks = 0;
int blackClicks = 0;


void doRed3()
{
  for(int d = 0; d < 4; d++)
  {
    LedControl::on(ledGreen);
    delay(100);
    LedControl::off(ledGreen);
    delay(200);
  }

  for(int d = 0; d < 4; d++)
  {
    LedControl::on(ledYellow);
    delay(100);
    LedControl::off(ledYellow);
    delay(200);
  }

  for(int d = 0; d < 4; d++)
  {
    LedControl::on(ledRed);
    delay(100);
    LedControl::off(ledRed);
    delay(200);
  }
}

void doBlack3()
{
  for(int d = 0; d < 4; d++)
  {
    LedControl::on(ledRed);
    delay(100);
    LedControl::off(ledRed);
    delay(200);
  }

  for(int d = 0; d < 4; d++)
  {
    LedControl::on(ledYellow);
    delay(100);
    LedControl::off(ledYellow);
    delay(200);
  }

  for(int d = 0; d < 4; d++)
  {
    LedControl::on(ledGreen);
    delay(100);
    LedControl::off(ledGreen);
    delay(200);
  }
}

void loop(void)
{
  if(millis() - msAlive >= MS_TOOFF)
  {
    Serial.println("Sleep");
    LedControl::allOff();
    apower.enterSleep();
    msAlive = millis();
  }else{
    buttonRed.Update();
    buttonBlack.Update();

    if(buttonRed.clicks != 0)
    {
      msAlive = millis();
    }
    redClicks = buttonRed.clicks;

    if(buttonBlack.clicks != 0)
    {
      msAlive = millis();
    }
    blackClicks = buttonBlack.clicks;

    switch(redClicks)
    {
      case 1:
        ledForward();
        Serial.println("RED 1");
        break;
      case 2:
        LedControl::off(led);
        led = ledRed;
        LedControl::on(led);
        Serial.println("RED 2");
        break;
      case 3:
        Serial.println("RED 3");
        LedControl::allOff();
        doRed3();
      case -1:
        Serial.println("RED Long");
        while(buttonRed.depressed)
        {
          ledForward();
          delay(50);
          buttonRed.Update();
        }
        break;

    }

    switch(blackClicks)
    {
      case 1:
        ledBackward();
        Serial.println("BLACK 1");
        break;
      case 2:
        LedControl::off(led);
        led = ledGreen;
        LedControl::on(led);
        Serial.println("BLACK 2");
        break;
      case 3:
        Serial.println("BLACK 3");
        LedControl::allOff();
        doBlack3();

      case -1:
        Serial.println("BLACK 3");
         while(buttonBlack.depressed)
        {
          ledBackward();
          delay(50);
          buttonBlack.Update();
        }
        break;
    }
  }

}
