#ifndef LEDCONTROL_H
#define LEDCONTROL_H

enum Leds{ ledRed, ledYellow, ledGreen};

class LedControl
{
  private:
  public:
    static void init();

    static void on(Leds led);
    static void off(Leds led);

    static void onlyOn(Leds led);
    static void allOff();


  protected:
  private:
};

#endif // LEDCONTROL_H
