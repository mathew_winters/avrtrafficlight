#include "LedControl.h"
#include "arduino/Arduino.h"



// LED Pins.
#undef RED_LED
#undef GREEN_LED
//#define RED_LED IO_D6
//#define YELLOW_LED IO_D5
//#define GREEN_LED IO_B2

#define RED_LED 6
#define YELLOW_LED 5
#define GREEN_LED 10


void LedControl::init()
{
  allOff();
  pinMode(RED_LED, OUTPUT);
  pinMode(YELLOW_LED, OUTPUT);
  pinMode(GREEN_LED, OUTPUT);
  allOff();

}

void LedControl::on(Leds led)
{
  switch(led)
  {
  case ledRed:
    digitalWrite(RED_LED, HIGH);
    break;
  case ledYellow:
    digitalWrite(YELLOW_LED, HIGH);
    break;
  case ledGreen:
    digitalWrite(GREEN_LED, HIGH);
    break;
  }
}

void LedControl::off(Leds led)
{
  switch(led)
  {
  case ledRed:
    digitalWrite(RED_LED, LOW);
    break;
  case ledYellow:
    digitalWrite(YELLOW_LED, LOW);
    break;
  case ledGreen:
    digitalWrite(GREEN_LED, LOW);
    break;
  }
}

void LedControl::onlyOn(Leds led)
{
  allOff();
  on(led);
}

void LedControl::allOff()
{
  digitalWrite(RED_LED, LOW);
  digitalWrite(YELLOW_LED, LOW);
  digitalWrite(GREEN_LED, LOW);

}
